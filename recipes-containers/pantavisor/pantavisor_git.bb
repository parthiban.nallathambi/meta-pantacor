SUMMARY = "Pantavisor is the container-based init system that makes it possible \
	   to enable your static firmware Linux device into a multi-function one."
HOMEPAGE = "https://gitlab.com/pantacor/pantavisor"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = "file://LICENSE;md5=9537d669f1ccfd74f3940b3bc1d3297d"

DEPENDS = "libthttp lxc libpvlogger"
RDEPENDS_${PN} = "lxc libpvlogger"

inherit cmake
SRC_URI = "git://gitlab.com/pantacor/${PN}.git;protocol=https \
	   file://0001-Add-CMake-for-pantavisor-compilation.patch \
           file://0001-Prevent-undefined-reference-to-makedev.patch \
	"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
CFLAGS_append = " -g -Wno-format-nonliteral -Wno-format-contains-nul -D_FILE_OFFSET_BITS=64 -fPIC -no-pie"
LDFLAGS +=" -Wl,--no-as-needed -lutil -Wl,--no-as-needed -ldl -Wl,--as-needed -static-libgcc "

do_configure_prepend() {
	${S}/gen_version.sh ${S} ${S}
}

do_install_append() {
	cd ${D}
	ln -fs usr/bin/${PN} init
}

FILES_${PN} += "/init"
