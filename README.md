# meta-pantacor Image Build

## Required packages on Ubuntu

This section provides required packages on an Ubuntu Linux distribution:

### Essentials

Packages needed to build an image for a headless system:

```shell
$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python python3 python3-pip python3-pexpect \
    xz-utils debianutils iputils-ping python-git repo bmap-tools
```

## Quick step

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master ; repo sync -j10; cd beagle/; TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env; bitbake pv-image-initramfs
```

## Download meta-pantacor project manifest

To easily manage different git repositories layers, meta-pantacor project is using [Android repo tool](https://source.android.com/source/using-repo),

First initialize repo specifying the project manifest and the corresponding branch:

```shell
$ repo init -u ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git -m conf/samples/beaglebone-yocto/pv-beaglebone.xml -b master
```

then checkout the project source tree:

```shell
$ repo sync -j10
```

## Configuring the project

meta-pantacor offers pre-configured machine templates, tested and ready to use.

- beaglebone black

```shell
$ cd beagle/
$ TEMPLATECONF=meta-pantacor/conf/samples/beaglebone-yocto source oe-init-build-env
```

## Build the project

### SD card image
```shell
$ bitbake pv-image-initramfs
```

## How to use meta-pantacor?

meta-pantacor provides only the recipes and components for pantavisor & it's
dependent components. This layer doesn't depend on any external Yocto layers
and functions as standalone layer. It gives plenty of flexibility to combine
with any BSP and application layer to produce pantavisor compatible software.

Note: This layer still depends on poky/oe-core for the image classes and initrd
creation.

Note: The samples in this layer directly uses beaglebone as example and will
be removed soon.

### Step 1:
Clone the meta-pantacor layer with (either with ssh or https)
```shell
git clone ssh://git@gitlab.com:parthiban.nallathambi/meta-pantacor.git
```

### Step 2:
Clone the BSP layer of your interest. For example, to clone RPi BSP layer,
```shell
git clone git://git.yoctoproject.org/meta-raspberrypi
```

### Step 3:
Rename the linux kernel recipe in recipes-kernel/linux/. Pantavisor needs few
kernel configuration bits enabled by default i.e like LXC dependencies.

Yocto doesn't provide a way to alias the bitbake recipe names. So the kernel
recipe should match the name in BSP layer. For RPi example,
```shell
git mv or mv linux-yocto-tiny_5.4.bbappend linux-raspberrypi_5.4.bbappend
```

Note: meta-pantacor doesn't restrict kernel version.


### Step 4: (WIP)
Build the pantavisor initramfs using,
```shell
bitbake pv-image-initramfs
```

and also provide the target image recipe name under a variable PANTA_IMAGE
(undecided and implementation in progress). This variable helps to identify
the image recipe name and builds the RFS, Kernel of target in pantavisor
compatible form.

This variable can be defined anywhere i.e conf/local.conf, machine.conf,
distro.conf ...

### Step 5:
Flash the build image to target stroage medium. Now the pantavisor system is
ready and should boot the Kernel + RFS on top of pantavisor.
